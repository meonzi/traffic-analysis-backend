create table aggregated_speed_entries
(
    entry_id             bigint generated by default as identity not null primary key,
    license_plate_number varchar(255)                            not null,
    date                 date                                    not null,
    average_speed        float                                   not null,
    minimum_speed        float                                   not null,
    maximum_speed        float                                   not null
);
package partners.bootcamp.repository

import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository
import partners.bootcamp.entity.AggregatedSpeedStats

@Repository("analytics-db")
interface AggregatedSpeedStatsRepository : CoroutineCrudRepository<AggregatedSpeedStats, Long>
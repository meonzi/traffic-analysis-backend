package partners.bootcamp.repository

import io.micronaut.data.annotation.Query
import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository
import partners.bootcamp.entity.AggregatedPassages
import partners.bootcamp.entity.Passage
import java.time.Instant
import java.util.UUID

@Repository("aggregation-db")
interface PassageRepository : CoroutineCrudRepository<Passage, String> {

    @Query("""
        select license_plate_number, count(*) as number_of_entries, avg(speed) as average_speed, min(speed) as minimum_speed, max(speed) as maximum_speed from passage group by license_plate_number
    """)
    suspend fun selectAggregatedResponses(start: Instant, end: Instant): Set<AggregatedPassages>

}
package partners.bootcamp.response

import io.micronaut.serde.annotation.Serdeable
import java.util.*

@Serdeable
data class CityDayStatsResponse(
    val date: Date,
    val averageSpeed: Float,
    val minSpeed: Float,
    val maxSpeed: Float,
)
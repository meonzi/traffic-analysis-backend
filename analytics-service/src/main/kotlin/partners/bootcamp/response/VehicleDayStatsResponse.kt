package partners.bootcamp.response

import io.micronaut.serde.annotation.Serdeable
import java.util.Date

@Serdeable
data class VehicleDayStatsResponse(
    val date: Date,
    val licensePlateNumber: String,
    val averageSpeed: Float,
    val minSpeed: Float,
    val maxSpeed: Float,
)

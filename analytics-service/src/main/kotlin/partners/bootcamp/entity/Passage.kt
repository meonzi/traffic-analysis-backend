package partners.bootcamp.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.Instant
import java.util.UUID

@Entity
@Table(name = "passages")
class Passage(
    @Id
    @Column(name = "passage_id", insertable = true, updatable = false, nullable = false)
    val id: String,

    @Column(name = "license_plate_number", insertable = true, updatable = true, nullable = true)
    var licensePlateNumber: String? = null,

    @Column(name = "speed", insertable = true, updatable = true, nullable = true)
    var speed: Float? = null,

    @Column(name = "timestamp", insertable = true, updatable = true, nullable = true)
    var timestamp: Instant? = null,

    @Column(name = "is_complete", insertable = true, updatable = true, nullable = false)
    var complete: Boolean = false
)
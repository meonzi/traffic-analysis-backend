package partners.bootcamp.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.LocalDate

@Entity
@Table(name = "aggregated_speed_entries")
class AggregatedSpeedStats(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "entry_id", insertable = false, updatable = false, nullable = false)
    val id: Long? = null,

    @Column(name = "license_plate_number", insertable = true, updatable = false, nullable = false)
    val licensePlateNumber: String,

    @Column(name = "date", insertable = true, updatable = false, nullable = false)
    val date: LocalDate,

    @Column(name = "average_speed", insertable = true, updatable = false, nullable = false)
    val averageSpeed: Float,

    @Column(name = "minimum_speed", insertable = true, updatable = false, nullable = false)
    val minimumSpeed: Float,

    @Column(name = "maximum_speed", insertable = true, updatable = false, nullable = false)
    val maximumSpeed: Float,
)
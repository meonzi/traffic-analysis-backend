package partners.bootcamp.entity

import io.micronaut.serde.annotation.Serdeable
import jakarta.persistence.Column

@Serdeable
class AggregatedPassages(
    @Column(name = "license_plate_number")
    val licensePlateNumber: String,

    @Column(name = "average_speed")
    val averageSpeed: Float,

    @Column(name = "minimum_speed")
    val minimumSpeed: Float,

    @Column(name = "maximum_speed")
    val maximumSpeed: Float,

    @Column(name = "number_of_entries")
    val numberOfEntries: Int
)
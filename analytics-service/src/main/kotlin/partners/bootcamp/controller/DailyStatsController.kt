package partners.bootcamp.controller

import io.micronaut.core.convert.format.Format
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import partners.bootcamp.response.CityDayStatsResponse
import partners.bootcamp.response.VehicleDayStatsResponse
import java.util.*

@Controller("/daily")
class DailyStatsController {

    @Get("/city")
    fun getDailyCityStats(
        @QueryValue @Format("yyyy-MM-dd") start: Date?,
        @QueryValue @Format("yyyy-MM-dd") end: Date?,
    ): HttpResponse<List<CityDayStatsResponse>> {
        return HttpResponse.ok(listOf(
            CityDayStatsResponse(
                Date(),
                43.2F,
                120.4F,
                3.0F
            )
        ))
    }

    @Get("/vehicle/{licensePlateNumber}")
    fun getDailyVehicleStats(
        licensePlateNumber: String,
        @QueryValue @Format("yyyy-MM-dd") start: Date?,
        @QueryValue @Format("yyyy-MM-dd") end: Date?,
    ): HttpResponse<List<VehicleDayStatsResponse>> {
        return HttpResponse.ok(listOf(
            VehicleDayStatsResponse(
                Date(),
                licensePlateNumber,
                43.2F,
                120.4F,
                3.0F
            )
        ))
    }
}
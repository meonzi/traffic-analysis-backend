# Traffic analysis backend

## Problem scenario
```
Implementujte systém pro analýzu rychlosti pohybu aut ve městě.
 
Ve městě jsou rozmístění brány. Brána má pro každý jízdní pruh kameru a radar. Brána obsahuje lokální uložiště dat a server připojený do internetu.
Po průjezdu auta se foto a data z radaru uloží do lokálního uložiště. Server periodicky odesílá fotky a data na centrální server, kde se vykoná další zpracování.
Centrální server provádí rozpoznání SPZ a spárování s daty z radaru. Z dat se denně pro každou SPZ vygeneruje statistika průměrné rychlosti pohybu po městě.
Ve městě není rychlost pohybu aut nijak omezena.
 
 
Co očekáváme jako výstup Vaší práce:
                1.            Analýzu celého řešení
                 - datový model všech komponent systému
                - sekvenční diagram toku dat přes systémy
                - analýzu řešte společně
                2.            Implementaci
                 - řešení na lokálu (celé spuštěno v Dockeru, případně potřebná část v Dockeru a zbytek v IDE).
                - v jazyku, který Vám je vlastní s libovolným frameworkem, databází etc.
                - systém je rozdělen na 3 komponenty (mikroservisa pro server brány, mikroservisa pro centrální server, rozpoznávání SPZ, mikroservisa pro statistiky)
                3.            Prezentaci celého řešení
 
Akceptační kritéria:
1. Operace OCR i dalšího zpracování není pro klienta serveru brány blokující operace
2. Systém garantuje, že všechna data ze serveru bran se dostanu na další zpracování
3. Systém je odolný vůči výpadkům centrálního serveru.
```

## High-level architecture
![High level architecture pseudo diagram](high-level-architecture.png "High level architecture pseudo diagram")

## Docker compose deployment setup

The "production" deployment can be started using the `docker-compose.deployment.yml` setup.
The whole docker compose cluster can be started with a single command:
```
docker compose -f ./docker-compose.deployment.yml up
```

After invoking the command, a RabbitMQ instance is started together with 2 PostgreSQL instances that are linked to the container services. 

The RabbitMQ management console can be accessed at http://localhost:15672 using the default credentials `guest:guest`.

The gateway is available at http://localhost:8081 and the analytics service API is available at http://localhost:8080.


## Docker compose dev setup

### RabbitMQ
``` 
host: localhost
port: 5672
```

### Aggregation DB
``` 
host: localhost
port: 5432
username: aggregation
password: aggregation
database: aggregation
```

### Analytics DB
``` 
host: localhost
port: 2345
username: analytics
password: analytics
database: analytics
```

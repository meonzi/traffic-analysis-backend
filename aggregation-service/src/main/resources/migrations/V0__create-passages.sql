create table passages
(
    passage_id           varchar(36)              not null primary key,
    license_plate_number varchar(12)              null,
    speed                double precision         null,
    timestamp            timestamp with time zone null,
    is_complete          boolean                  not null default false
);

create index ix_passages_complete on passages (is_complete);

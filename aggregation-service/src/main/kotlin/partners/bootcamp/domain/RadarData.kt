package partners.bootcamp.domain

import java.time.Instant

data class RadarData(
    val speed: Float,
    val timestamp: Instant
)
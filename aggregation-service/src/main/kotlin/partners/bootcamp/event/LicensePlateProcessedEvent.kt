package partners.bootcamp.event

import io.micronaut.serde.annotation.Serdeable
import java.util.UUID

@Serdeable
data class LicensePlateProcessedEvent(
    val id: UUID,
    val licensePlateNumber: String
)
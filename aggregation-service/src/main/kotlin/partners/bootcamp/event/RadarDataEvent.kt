package partners.bootcamp.event

import io.micronaut.serde.annotation.Serdeable
import java.time.Instant
import java.util.UUID

@Serdeable
class RadarDataEvent(
    val id: UUID,
    val speed: Float,
    val timestamp: Instant
)
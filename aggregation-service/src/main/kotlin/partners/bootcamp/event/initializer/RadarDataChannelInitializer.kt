package partners.bootcamp.event.initializer

import com.rabbitmq.client.Channel
import io.micronaut.rabbitmq.connect.ChannelInitializer
import jakarta.inject.Singleton

const val RADAR_DATA_QUEUE = "RADAR_DATA_QUEUE"

@Singleton
class RadarDataChannelInitializer : ChannelInitializer() {

    override fun initialize(channel: Channel, name: String?) {
        channel.queueDeclare(RADAR_DATA_QUEUE, true,  false, false, null)
    }

}
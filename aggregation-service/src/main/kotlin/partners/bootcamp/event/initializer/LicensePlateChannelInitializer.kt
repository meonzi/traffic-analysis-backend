package partners.bootcamp.event.initializer

import com.rabbitmq.client.Channel
import io.micronaut.rabbitmq.connect.ChannelInitializer
import jakarta.inject.Singleton

const val LICENSE_PLATE_PROCESSED_QUEUE = "LICENSE_PLATE_PROCESSED_QUEUE"

@Singleton
class LicensePlateChannelInitializer : ChannelInitializer() {

    override fun initialize(channel: Channel, name: String?) {
        channel.queueDeclare(LICENSE_PLATE_PROCESSED_QUEUE, true,  false, false, null)
    }

}
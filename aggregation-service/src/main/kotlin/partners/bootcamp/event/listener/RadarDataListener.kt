package partners.bootcamp.event.listener

import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import io.micronaut.rabbitmq.bind.RabbitAcknowledgement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import partners.bootcamp.domain.RadarData
import partners.bootcamp.event.RadarDataEvent
import partners.bootcamp.event.initializer.RADAR_DATA_QUEUE
import partners.bootcamp.service.PassageService

@RabbitListener
class RadarDataListener(private val service: PassageService) {

    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)

    @Queue(RADAR_DATA_QUEUE)
    fun handleRadarDataEvent(event: RadarDataEvent, acknowledgement: RabbitAcknowledgement) {
        scope.launch {
            // TODO: Validation, transformation, ...
            service.handleRadarData(event.id, RadarData(event.speed, event.timestamp))
            acknowledgement.ack()
        }
    }
}
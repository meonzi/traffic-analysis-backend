package partners.bootcamp.event.listener

import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import io.micronaut.rabbitmq.bind.RabbitAcknowledgement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import partners.bootcamp.event.LicensePlateProcessedEvent
import partners.bootcamp.event.initializer.LICENSE_PLATE_PROCESSED_QUEUE
import partners.bootcamp.service.PassageService

@RabbitListener
class LicensePlateProcessedListener(private val service: PassageService) {

    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)

    @Queue(LICENSE_PLATE_PROCESSED_QUEUE)
    fun handleLicensePlateProcessedEvent(event: LicensePlateProcessedEvent, acknowledgement: RabbitAcknowledgement) {
        scope.launch {
            service.handleProcessedLicensePlate(event.id, event.licensePlateNumber)
            acknowledgement.ack()
        }
    }
}
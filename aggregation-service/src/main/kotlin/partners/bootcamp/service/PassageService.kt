package partners.bootcamp.service

import jakarta.inject.Singleton
import partners.bootcamp.domain.RadarData
import partners.bootcamp.entity.Passage
import partners.bootcamp.repository.PassageRepository
import java.util.*

@Singleton
class PassageService(private val repository: PassageRepository) {

    suspend fun handleProcessedLicensePlate(passageId: UUID, licensePlateNumber: String) {
        val entity = createPassageIfNotExists(passageId)
        val updated = entity.apply {
            this.licensePlateNumber = licensePlateNumber
            this.complete = entity.speed != null && entity.timestamp != null
        }

        repository.update(updated)
    }

    suspend fun handleRadarData(passageId: UUID, data: RadarData) {
        val entity = createPassageIfNotExists(passageId)
        val updated = entity.apply {
            this.speed = data.speed
            this.timestamp = data.timestamp
            this.complete = entity.licensePlateNumber != null
        }

        repository.update(updated)
    }

    private suspend fun createPassageIfNotExists(id: UUID): Passage {
        return repository.findById(id.toString()) ?: repository.save(Passage(id.toString()))
    }

}
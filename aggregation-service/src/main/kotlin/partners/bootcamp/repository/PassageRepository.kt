package partners.bootcamp.repository

import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository
import partners.bootcamp.entity.Passage
import java.util.UUID

@Repository
interface PassageRepository : CoroutineCrudRepository<Passage, String>
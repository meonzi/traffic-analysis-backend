package partners.bootcamp.service

import jakarta.inject.Singleton
import kotlinx.coroutines.delay
import kotlin.math.absoluteValue
import kotlin.time.Duration.Companion.seconds

private const val MAX_LICENSE_PLATES: Int = 20

@Singleton
class OCRService {

    suspend fun imageToLicensePlateNumber(imageDataBase64: String): String {
        delay((3..10).random().seconds)

        val randomId = imageDataBase64.hashCode().absoluteValue % MAX_LICENSE_PLATES
        return "VEHICLE-${randomId}"
    }
}

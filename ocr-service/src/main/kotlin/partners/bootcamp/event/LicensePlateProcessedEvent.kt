package partners.bootcamp.event

import io.micronaut.serde.annotation.Serdeable
import java.util.*

@Serdeable
data class LicensePlateProcessedEvent(
    val uuid: UUID,
    val licensePlateNumber: String,
)

package partners.bootcamp.event

import io.micronaut.serde.annotation.Serdeable
import java.util.UUID

@Serdeable
data class LicensePlateScannedEvent(
    val uuid: UUID,
    val imageDataBase64: String,
)
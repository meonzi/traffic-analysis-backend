package partners.bootcamp.event

import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient(LICENSE_PLATE_PROCESSED_EXCHANGE_NAME)
interface LicensePlateProcessedEventProducer {

    @Binding(LICENSE_PLATE_PROCESSED_ROUTING_KEY)
    fun send(data: LicensePlateProcessedEvent)

}
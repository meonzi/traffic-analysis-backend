package partners.bootcamp.event

import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import kotlinx.coroutines.runBlocking
import partners.bootcamp.service.OCRService

@RabbitListener
class LicensePlateScannedListener(
    private val ocrService: OCRService,
    private val licensePlateProcessedEventProducer: LicensePlateProcessedEventProducer
) {

    @Queue("LICENSE_PLATE_SCANNED_QUEUE")
    fun receive(event: LicensePlateScannedEvent) {
        runBlocking {
            val licensePlate = ocrService.imageToLicensePlateNumber(event.imageDataBase64)

            licensePlateProcessedEventProducer.send(LicensePlateProcessedEvent(event.uuid, licensePlate))
        }
    }
}
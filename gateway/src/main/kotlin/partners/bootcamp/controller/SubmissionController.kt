package partners.bootcamp.controller

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import partners.bootcamp.request.SubmitRadarDataRequest
import partners.bootcamp.service.PassageService

@Controller
class SubmissionController(private val passageService: PassageService) {

    @Post("/submit", consumes = [MediaType.MULTIPART_FORM_DATA], produces = [MediaType.APPLICATION_JSON])
    suspend fun submit(@Body request: SubmitRadarDataRequest): HttpResponse<Unit> {
        passageService.submit(
            request.photo.toByteArray(),
            request.speed,
            request.timestamp
        )

        return HttpResponse.ok()
    }

}
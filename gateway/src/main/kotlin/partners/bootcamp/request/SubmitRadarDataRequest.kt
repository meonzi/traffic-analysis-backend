package partners.bootcamp.request

import io.micronaut.serde.annotation.Serdeable
import java.time.Instant

@Serdeable
data class SubmitRadarDataRequest(
    val photo: String,
    val speed: Float,
    val timestamp: Instant
)
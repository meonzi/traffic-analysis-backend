package partners.bootcamp.event

import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient(RADAR_DATA_EXCHANGE_NAME)
interface RadarDataEventProducer {

    @Binding(RADAR_DATA_ROUTING_KEY)
    fun send(data: RadarDataEvent)

}
package partners.bootcamp.event

import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient(LICENSE_PLATE_SCANNED_EXCHANGE_NAME)
interface LicensePlateScannedEventProducer {

    @Binding(LICENSE_PLATE_SCANNED_ROUTING_KEY)
    fun send(data: LicensePlateScannedEvent)

}
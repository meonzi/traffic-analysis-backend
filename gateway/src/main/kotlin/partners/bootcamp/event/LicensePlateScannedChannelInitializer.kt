package partners.bootcamp.event

import com.rabbitmq.client.Channel
import io.micronaut.rabbitmq.connect.ChannelInitializer
import jakarta.inject.Singleton

const val LICENSE_PLATE_SCANNED_EXCHANGE_NAME = "LICENSE_PLATE_SCANNED_EXCHANGE"
const val LICENSE_PLATE_SCANNED_QUEUE_NAME = "LICENSE_PLATE_SCANNED_QUEUE"
const val LICENSE_PLATE_SCANNED_ROUTING_KEY = "events"

@Singleton
class LicensePlateScannedChannelInitializer : ChannelInitializer() {

    override fun initialize(channel: Channel, name: String?) {
        channel.exchangeDeclare(LICENSE_PLATE_SCANNED_EXCHANGE_NAME, "direct")
        channel.queueDeclare(LICENSE_PLATE_SCANNED_QUEUE_NAME, true,  false, false, null)
        channel.queueBind(LICENSE_PLATE_SCANNED_QUEUE_NAME, LICENSE_PLATE_SCANNED_EXCHANGE_NAME, LICENSE_PLATE_SCANNED_ROUTING_KEY)
    }

}
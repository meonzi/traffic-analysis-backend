package partners.bootcamp.event

import com.rabbitmq.client.Channel
import io.micronaut.rabbitmq.connect.ChannelInitializer
import jakarta.inject.Singleton

const val RADAR_DATA_EXCHANGE_NAME = "RADAR_DATA_EXCHANGE"
const val RADAR_DATA_QUEUE_NAME = "RADAR_DATA_QUEUE"
const val RADAR_DATA_ROUTING_KEY = "events"

@Singleton
class RadarDataChannelInitializer : ChannelInitializer() {

    override fun initialize(channel: Channel, name: String?) {
        channel.exchangeDeclare(RADAR_DATA_EXCHANGE_NAME, "direct")
        channel.queueDeclare(RADAR_DATA_QUEUE_NAME, true,  false, false, null)
        channel.queueBind(RADAR_DATA_QUEUE_NAME, RADAR_DATA_EXCHANGE_NAME, RADAR_DATA_ROUTING_KEY)
    }

}
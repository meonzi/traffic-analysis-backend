package partners.bootcamp.service

import jakarta.inject.Singleton
import partners.bootcamp.event.RadarDataEvent
import partners.bootcamp.event.RadarDataEventProducer
import java.time.Instant
import java.util.UUID

@Singleton
class RadarDataService(private val eventProducer: RadarDataEventProducer) {

    fun submit(id: UUID, speed: Float, timestamp: Instant) {
        val event = RadarDataEvent(id, speed, timestamp)

        eventProducer.send(event)
    }
}
package partners.bootcamp.service

import jakarta.inject.Singleton
import java.time.Instant
import java.util.UUID

@Singleton
class PassageService(private val radarDataService: RadarDataService, private val licensePlateScanService: LicensePlateScanService) {

    fun submit(imageData: ByteArray, speed: Float, timestamp: Instant = Instant.now()) {
        val id = UUID.randomUUID()

        licensePlateScanService.submit(id, imageData)
        radarDataService.submit(id, speed, timestamp)
    }
}

package partners.bootcamp.service

import jakarta.inject.Singleton
import partners.bootcamp.event.LicensePlateScannedEvent
import partners.bootcamp.event.LicensePlateScannedEventProducer
import java.util.Base64
import java.util.UUID

@Singleton
class LicensePlateScanService(private val eventProducer: LicensePlateScannedEventProducer) {

    fun submit(id: UUID, imageData: ByteArray) {
        val b64Encoded = Base64.getUrlEncoder().encode(imageData)
        val event = LicensePlateScannedEvent(
            id,
            String(b64Encoded),
        )

        eventProducer.send(event)
    }
}